package ru.t1.volkova.tm.api.service;

import ru.t1.volkova.tm.api.repository.IUserOwnedRepository;
import ru.t1.volkova.tm.model.AbstractUserOwnedModel;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M> {

    M create(String userId, String name, String description);

}
