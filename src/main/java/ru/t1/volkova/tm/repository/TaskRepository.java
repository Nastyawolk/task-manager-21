package ru.t1.volkova.tm.repository;

import ru.t1.volkova.tm.api.repository.ITaskRepository;
import ru.t1.volkova.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        return findAll(userId)
                .stream()
                .filter(t -> t.getProjectId() != null && projectId.equals(t.getProjectId()))
                .collect(Collectors.toList());
    }

}
