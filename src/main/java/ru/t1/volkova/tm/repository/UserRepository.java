package ru.t1.volkova.tm.repository;

import ru.t1.volkova.tm.api.repository.IUserRepository;
import ru.t1.volkova.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findOneByLogin(final String login) {
        return records
                .stream()
                .filter(r -> login.equals(r.getLogin()))
                .findFirst().orElse(null);
    }

    @Override
    public User findOneByEmail(final String email) {
        return records
                .stream()
                .filter(r -> email.equals(r.getEmail()))
                .findFirst().orElse(null);
    }

    @Override
    public Boolean isLoginExist(final String login) {
        return records
                .stream()
                .anyMatch(r -> login.equals(r.getLogin()));
    }

    @Override
    public Boolean isEmailExist(final String email) {
        return records
                .stream()
                .anyMatch(r -> email.equals(r.getEmail()));
    }

}
