package ru.t1.volkova.tm.command.project;

import ru.t1.volkova.tm.enumerated.ProjectSort;
import ru.t1.volkova.tm.model.Project;
import ru.t1.volkova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    private static final String DESCRIPTION = "Show list projects.";

    private static final String NAME = "project-list";

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(ProjectSort.values()));
        final String sortType = TerminalUtil.nextLine();
        final ProjectSort sort = ProjectSort.toSort(sortType);
        final String userId = getUserId();
        final List<Project> projects;
        if (sort == null) projects = projectService().findAll(userId);
        else projects = projectService().findAll(userId, sort.getComparator());
        renderProjects(projects);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
