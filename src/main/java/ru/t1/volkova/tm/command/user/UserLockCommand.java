package ru.t1.volkova.tm.command.user;

import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand {

    private static final String DESCRIPTION = "Lock user";

    private static final String NAME = "user-lock";

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN");
        final String login = TerminalUtil.nextLine();
        getUserService().lockUserByLogin(login);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
